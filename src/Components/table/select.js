import React, { Component } from 'react';
import { DatePicker, Select } from 'antd';
const { Option } = Select;

 
const { RangePicker } = DatePicker;
export default class select extends Component {
  render() {
    return (
      <div className='d-flex justify-content-between'>
        <RangePicker renderExtraFooter={() => 'pass month'} />
        <Select defaultValue='All Launches' style={{ width: 200 }} >
          <Option value='All Launches'>All Launches</Option>
          <Option value='Upcoming Launches'>Upcoming Launches</Option>
          <Option value='Successful Launches'>Successful Launches</Option>
          <Option value='Failed Lunches'>Failed Lunches</Option>
        </Select>
      </div>
    );
  }
}
