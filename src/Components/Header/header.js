import React, { Component } from 'react'
import './header.css'
import logo from '../../assets/logoHeader.png' 
export default class header extends Component {
    render() {
        return (
            <div className="App">
            <header >
              <img src={logo} className="" alt="logo" />
            </header> 
            <hr></hr>
          </div>
        )
    }
}
