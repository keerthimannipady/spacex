import React from 'react';

import './App.css';
import { Table, Modal, Button } from 'antd';

import axios from 'axios';
import logo from '../src/assets/logoHeader.png';
import Header from '../src/Components/Header/header';
import Selection from '../src/Components/table/select';
const TableColumns = (showModal) => [
  {
    title: 'No',
    dataIndex: 'flight_number',
    key: 'flight_number',
  },
  {
    title: 'Launched (UTC)',
    dataIndex: 'launch_date_utc',
    key: 'launch_date_utc',
  },
  {
    title: 'Mission',
    dataIndex: 'mission_name',
    key: 'mission_name',
    render: (data) => <span className='text-dark'>{data}</span>,
  },
  {
    title: 'Orbit',
    dataIndex: 'mission_name',
    key: 'OR_CUST_NAME_ADDR',
    render: (data) => <span className='text-dark'>LEO</span>,
  },
  {
    title: 'Launch Status',
    dataIndex: 'launch_success',
    key: 'OR_CUST_NAME_ADDR',
    render: (data, fullData) => (
      <Button
        style={{ backgroundColor: 'yellow', border: 'none' }}
        className='text-danger rounded-pill'
        onClick={() => showModal(fullData)}
      >
        Failed
      </Button>
    ),
  },
  {
    title: 'Rocket',
    dataIndex: 'OR_CUST_NAME_ADDR',
    key: 'OR_CUST_NAME_ADDR',
    render: (data) => <span className='text-dark'>Falcon 9</span>,
  },
];

function App() {
  const [tableData, setTableData] = React.useState([]);
  const [loading, setIsLoading] = React.useState(true);
  const [isModalVisible, setIsModalVisible] = React.useState(false);
  const [selectedData, setSelectedData] = React.useState({});

  React.useEffect(() => {
    axios
      .get('https://api.spacexdata.com/v3/launches/')
      .then((res) => {
        setTableData(res.data);
      })
      .catch((error) => {
        console.log(error.message);
      })
      .finally(() => setIsLoading(false));
  }, []);

  const showModal = (fullData) => {
    setSelectedData(fullData);
    setIsModalVisible(true);
  };

  const closeModal = () => setIsModalVisible(false);

  return (
    <div className='App'>
      <div className='container'>
        <Header />
        <Selection />
        <Table
          columns={TableColumns(showModal)}
          dataSource={tableData}
          loading={loading}
        />
        <Modal
          title=''
          visible={isModalVisible}
          onOk={closeModal}
          onCancel={closeModal}
        >
          <div className='d-flex ' style={{alignItems:"baseline"}}>
            <div>
              <img src={logo} height='100px' width='150' alt=""></img>
            </div>
            <div>
              {selectedData.mission_name}
            </div>
          </div>
          <p>paragraph content that mapped from backend</p>

          <label style={{width:"50%"}}>Flight number</label><label > {selectedData.flight_number}</label>
          <hr/> 

          <label style={{width:"50%"}}>Mission Name</label><label >CRS-1</label>
          <hr/>
          
          <label style={{width:"50%"}}>Rocket Type</label><label >V1.0</label>
          <hr/>
          
          <label style={{width:"50%"}}>Rocket Name</label><label >Falcon 9</label>
          <hr/>
          
          <label style={{width:"50%"}}>Manufacturer</label><label >Spacex</label>
          <hr/>
          
          <label style={{width:"50%"}}>Nationality</label><label >Spacex</label>
          <hr/>
          
          <label style={{width:"50%"}}>Launch Date</label><label >08 October 2012 00:35</label>
          <hr/>
          
          <label style={{width:"50%"}}>Payload type</label><label >Dragon 1.0</label>
          <hr/>
          
          <label style={{width:"50%"}}>Orbit</label><label >ISS</label>
          <hr/>
          
          <label style={{width:"50%"}} >Launch Site</label><label >CCAFS SLC 40</label>
          <hr/>
          
        </Modal>
      </div>
    </div>
  );
}

export default App;
